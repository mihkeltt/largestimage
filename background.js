"use strict";

browser.browserAction.onClicked.addListener(() => { currentTab(); });

browser.menus.create({
  id: "toTheRight",
  title: browser.i18n.getMessage("menuItemShowToTheRight"),
  contexts: ["browser_action"]
}, null);

browser.menus.onClicked.addListener((info, tab) => {
  if (info.menuItemId == "toTheRight") {
    toTheRight();
  }
});

var currentTab = () => {
  browser.tabs
    .query({
      currentWindow: true,
      active: true
    })
    .then(showTabLargestImage)
    .catch(onError);
};

var toTheRight = () => {
  browser.tabs
    .query({ currentWindow: true, active: true })
    .then((activeTabs) => {
      var tab = activeTabs[0];

      browser.tabs
        .query({ currentWindow: true })
        .then((currentWindowTabs) => {
          if (tab.index + 1 >= currentWindowTabs.length)
            return;

          showTabLargestImage(currentWindowTabs.slice(tab.index + 1, currentWindowTabs.length));
        })
        .catch(onError);
    })
    .catch(onError);
};

browser.commands.onCommand.addListener(function (command) {
  if (command == "current-tab") {
    currentTab();
  }
  else if (command == "to-the-right") {
    toTheRight();
  }
});

function onError(error) {
  console.error(`Error: ${error}`);
}

function showTabLargestImage(tabs) {
  for (let tab of tabs) {
    browser.tabs
      .sendMessage(tab.id, { method: "showImage", tab: tab })
      .then(response => { })
      .catch(onError);
  }
}