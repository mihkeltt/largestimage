"use strict";

var showImage = false;

browser.runtime.onMessage.addListener(request => {
  let method = request.method;
  let tab = request.tab;

  if (method == "showImage") {
    if (isWindowLoaded()) {
      showLargestImage();
    }
    else {
      showImage = true;
    }
  }

  return Promise.resolve();
});

window.addEventListener("load", function () {
  if (showImage) {
    showLargestImage();
  }
}, false);

var isWindowLoaded = function () {
  return document.readyState == "complete";
}

var showLargestImage = function () {
  var imageUrl = getLargestImage();

  if (imageUrl == 0) {
    // No images
  }
  else if (imageUrl == -1) {
    //window.history.back();
  }
  else {
    window.location.href = imageUrl;
  }
}

var getLargestImage = function () {
  var nodelist = document.getElementsByTagName("img");
  var src = document.URL;
  var area = 0;

  if (!nodelist.length) {
    return 0;
  }

  for (var i = 0; i < nodelist.length; i++) {
    if ((nodelist.item(i).height * nodelist.item(i).width) > area) {
      area = nodelist.item(i).height * nodelist.item(i).width
      src = nodelist.item(i).getAttribute("src");
    }
  }

  var path = joinPaths(document.URL, src);
  if (path == document.URL) {
    return -1;
  }
  else {
    return path;
  }
}

var joinPaths = function (baseUrl, relativeUrl) {
  if (relativeUrl.indexOf("http") == 0 || relativeUrl.indexOf("//") == 0) {
    return relativeUrl;
  }

  if (relativeUrl.charAt(0) == "/") {
    baseUrl = baseUrl.substr(0, baseUrl.indexOf("/", baseUrl.indexOf(".")));
    relativeUrl = relativeUrl.substr(1, relativeUrl.length - 1);
  }
  else {
    var qm = baseUrl.indexOf("?");

    if (qm + 1) {
      baseUrl = baseUrl.substr(0, qm);
    }

    baseUrl = baseUrl.substr(0, baseUrl.lastIndexOf("/"));
  }

  return baseUrl + "/" + relativeUrl;
};